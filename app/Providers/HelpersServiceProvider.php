<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelpersServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach (glob(app_path() . '/Helpers/*.functions.php') as $file) {
            /** @noinspection PhpIncludeInspection */
            require_once($file);
        }

        if (env('APP_ENV') === 'testing') {
            foreach (glob(base_path() . '/tests/Helpers/*.functions.php') as $file) {
                /** @noinspection PhpIncludeInspection */
                require_once($file);
            }
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
