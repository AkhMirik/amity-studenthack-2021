<?php

namespace App\Http\Controllers;

use App\Helpers\TelegramAgent;
use App\Helpers\TelegramAuth;
use App\Models\Person;
use App\User;
use Auth;

class TelegramAuthController extends Controller
{
    protected $telegram;

    public function __construct(TelegramAuth $telegram)
    {
        $this->telegram = $telegram;
    }

    public function handleTelegramCallback()
    {
        if ($this->telegram->validate()) {
            $telegram = $this->telegram->user();
            $user = Person::whereTelegramId($telegram['id'])->first();

            // No user with this telegram id (register)
            if (!$user) {
                $user = Person::createNew($telegram);
            }

            Auth::guard('person')->login($user, true);

            return redirect(route('map'));
        }

        return abort(401);
    }

    public function isLoggedIn()
    {
        return responseJson([
            'success' => true
        ]);
    }
}
