<?php

use App\Models\Document;

function documents_path($path = '')
{
    if ($path instanceof Document) {
        $path = $path->path;
    }

    return storage_path('app/public/' . trim($path, '/'));
}
