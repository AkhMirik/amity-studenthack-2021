<?php

namespace App\Models;

use App\Jobs\DeliverIssue;
use Auth;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Str;

class Issue extends Model
{
    use HasFactory;

    protected static $unguarded = true;

    const STATE_NEW = 0;
    const STATE_ACTIVE = 1;
    const STATE_SOLVED = 2;
    const STATE_REJECTED = 3;

    public static function getStates(): array
    {
        return [
            self::STATE_NEW      => "Yangi",
            self::STATE_ACTIVE   => "Ko'rib chiqilmoqda",
            self::STATE_SOLVED   => "Hal qilindi",
            self::STATE_REJECTED => "Rad etildi",
        ];
    }

    protected static function boot()
    {
        parent::boot();
    }


    public function getStateTextAttribute()
    {
        return self::getStates()[$this->state];
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    public function votes()
    {
        return $this->hasMany(IssuePerson::class, 'issue_id', 'id');
    }

    public function getHasVotedAttribute()
    {
        if (!Auth::guard('person')->check()) {
            return -1;
        }

        return $this->hasVoteFor(Auth::guard('person')->id());
    }

    public function hasVoteFor($person_id)
    {
        return !!$this->votes->where('person_id', $person_id)->count();
    }

    public function people()
    {
        return $this->belongsToMany(IssuePerson::class, 'issue_person', 'issue_id', 'person_id');
    }

    public function attachDocument(UploadedFile $document, $kind = Document::KIND_REQUEST)
    {
        if ($kind === null){
            $kind = Document::KIND_REQUEST;
        }

        $name = $document->getClientOriginalName();
        $ext = $document->extension();

        $path = Str::random() . '.' . $ext;

        $document->move(documents_path(), $path);

        return $this->documents()->create([
            'name' => $name,
            'path' => $path,
            'type' => Document::analyzeType($ext),
            'kind' => $kind,
        ]);
    }
}
