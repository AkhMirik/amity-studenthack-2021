<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected static $unguarded = true;

    const TYPE_DOCUMENT = 0;
    const TYPE_IMAGE = 1;
    const TYPE_VIDEO = 2;
    const TYPE_OTHER = 3;

    const KIND_REQUEST = 0;
    const KIND_RESPONSE = 1;

    public static function getTypes(): array
    {
        return [
            self::TYPE_DOCUMENT,
            self::TYPE_IMAGE,
            self::TYPE_OTHER,
        ];
    }

    public static function analyzeType($extension): int
    {
        $images = collect(['jpg', 'jpeg', 'png', 'svg']);
        $documents = collect(['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf']);
        $videos = collect(['mp4', 'mkv', 'avi']);

        $type = self::TYPE_OTHER;

        if ($images->contains($extension)) {
            $type = self::TYPE_IMAGE;
        }

        if ($documents->contains($extension)) {
            $type = self::TYPE_DOCUMENT;
        }

        if ($videos->contains($extension)) {
            $type = self::TYPE_VIDEO;
        }

        return $type;
    }

    public function getFullPathAttribute(): string
    {
        return documents_path($this->path);
    }

    public function issue()
    {
        $this->belongsTo(Issue::class);
    }
}
