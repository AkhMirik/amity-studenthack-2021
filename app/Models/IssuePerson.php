<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IssuePerson extends Model
{
    use HasFactory;

    protected static $unguarded = true;
    public $timestamps = false;

    protected $table = 'issue_person';
}
