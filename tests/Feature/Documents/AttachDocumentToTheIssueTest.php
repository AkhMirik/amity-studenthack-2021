<?php

namespace Tests\Feature\Documents;

use App\Models\Document;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AttachDocumentToTheIssueTest extends TestCase
{

    /** @test */
    public function person_can_attach_document_to_issue()
    {
        [$issue, $category, $person] = genIssue();
        $file = makeUploadedFile();

        $this->postJson(route('documents.store'), [
            'person_id' => $person->id,
            'issue_id'  => $issue->id,
            'file'      => $file,
        ])
            ->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseCount('documents', 1);

        $issue->refresh();
        $document = Document::first();

        $this->assertFileExists(documents_path($document));
        $this->assertCount(1, $issue->documents);
    }
}
