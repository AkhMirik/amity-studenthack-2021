<?php

namespace Tests\Feature\Issues;

use App\Models\Issue;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetIssuesTest extends TestCase
{

    /** @test */
    public function person_can_see_all_issues()
    {
        [$issue1] = genIssue();
        [$issue2] = genIssue();

        $this
            ->getJson(route('issues.index'))
            ->assertStatus(Response::HTTP_OK)
            ->assertJson([
                'data' => [
                    ['id' => $issue1->id],
                    ['id' => $issue2->id],
                ]
            ]);
    }

    /** @test */
    public function person_can_get_issues_by_category()
    {
        [$issue1, $category1] = genIssue();
        genIssue();

        $endpoint = route('issues.index', ['category_id' => $category1->id]);

        $this
            ->getJson($endpoint)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1, 'data')
            ->assertJson([
                'data' => [
                    ['id' => $issue1->id]
                ]
            ]);

    }

    /** @test */
    public function person_can_get_issues_by_person()
    {
        [$issue1, $category1, $person1] = genIssue();
        genIssue();

        $endpoint = route('issues.index', ['person_id' => $person1->id]);

        $this
            ->getJson($endpoint)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1, 'data')
            ->assertJson([
                'data' => [
                    ['id' => $issue1->id],
                ]
            ]);

    }

    /** @test */
    public function person_can_get_issues_by_state()
    {
        /** @var Issue $issue */
        $issue = genIssue()[0];

        $this->assertStateFilter(Issue::STATE_NEW, 1);
        $this->assertStateFilter(Issue::STATE_ACTIVE, 0);
        $this->assertStateFilter(Issue::STATE_SOLVED, 0);

        $issue->update(['state' => Issue::STATE_ACTIVE]);

        $this->assertStateFilter(Issue::STATE_NEW, 0);
        $this->assertStateFilter(Issue::STATE_ACTIVE, 1);
        $this->assertStateFilter(Issue::STATE_SOLVED, 0);

        $issue->update(['state' => Issue::STATE_SOLVED]);

        $this->assertStateFilter(Issue::STATE_NEW, 0);
        $this->assertStateFilter(Issue::STATE_ACTIVE, 0);
        $this->assertStateFilter(Issue::STATE_SOLVED, 1);
    }

    private function assertStateFilter($state, $count)
    {
        return $this
            ->getJson(route('issues.index', compact('state')))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount($count, 'data');
    }
}
