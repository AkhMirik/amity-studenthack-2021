<?php

namespace Tests\Feature\Issues;

use App\Models\Issue;
use Arr;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateANewIssueTest extends TestCase
{

    /** @test */
    public function person_can_add_new_issue()
    {
        $category = createCategory();
        $person = createPerson();

        $issue = makeIssue([
            'category_id' => $category->id,
            'person_id'   => $person->id,
        ]);

        Arr::forget($issue, 'state');

        $this->postJson(route('issues.store'), $issue)
            ->assertStatus(Response::HTTP_CREATED);

        $issue['state'] = Issue::STATE_NEW;

        $this->assertDatabaseHas('issues', $issue);
    }
}
