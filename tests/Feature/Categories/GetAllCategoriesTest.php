<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetAllCategoriesTest extends TestCase
{

    /** @test */
    public function person_can_get_all_categories_list()
    {
        $category = Category::factory()->create();

        $this->getJson(route('categories.index'))
            ->assertStatus(Response::HTTP_OK)
            ->assertSee($category->name);
    }
}
