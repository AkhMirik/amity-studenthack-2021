<?php

use App\Models\Category;
use App\Models\Issue;
use App\Models\Person;
use Illuminate\Http\UploadedFile;
use Spatie\TemporaryDirectory\TemporaryDirectory;

function createCategory($attributes = []): Category
{
    return Category::factory()->create($attributes);
}

function createPerson($attributes = []): Person
{
    return Person::factory()->create($attributes);
}

function makeIssue($attributes = []): array
{
    $array = Issue::factory()->make($attributes)->toArray();
    Arr::forget($array, ['region', 'district', 'location']);
    return $array;
}

function makeUploadedFile()
{
    $temporary_directory = (new TemporaryDirectory())->create();
    $path = $temporary_directory->path('image.jpg');

    File::copy(public_path('dumps/image.jpg'), $path);

    return new UploadedFile($path, 'image.jpg', 'image/jpg', null, true);
}
