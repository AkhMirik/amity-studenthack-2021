<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // Real data for production
            RealDataSeeder::class,
            AdminTablesSeeder::class,

            // Dump data for testing
            FakeDataSeeder::class,
        ]);
    }
}
