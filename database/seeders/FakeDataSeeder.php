<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Issue;
use App\Models\Person;
use Illuminate\Database\Seeder;

class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Person::factory(1)
//            ->hasIssues(1)
//            ->create();

        Person::factory(100)
            ->hasIssues(10)
            ->create();
    }
}
