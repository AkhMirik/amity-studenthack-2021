<?php

use App\Models\Document;
use App\Models\Issue;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MainMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('webhook_url')->default('');
        });

        Schema::create('people', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('telegram_id');
            $table->string('phone');

            $table->rememberToken();
            $table->string('tg_username')->nullable();
            $table->string('password')->nullable();

            $table->timestamps();
        });

        Schema::create('issues', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('person_id');

            $table->smallInteger('state')->default(Issue::STATE_NEW);

            $table->string('title');
            $table->text('description');

            $table->boolean('delivered')->default(false);
            $table->string('response_message')->nullable();

            $table->text('location');
            $table->string('long');
            $table->string('lat');
            $table->string('region');
            $table->string('district');

            $table->timestamps();

        });

        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('issue_id');

            $table->smallInteger('type');
            $table->smallInteger('kind')->default(Document::KIND_REQUEST);

            $table->string('name');
            $table->string('path');

            $table->timestamps();

        });

        Schema::create('issue_person', function (Blueprint $table) {
            $table->unsignedBigInteger('issue_id');
            $table->unsignedBigInteger('person_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('people');
        Schema::dropIfExists('issues');
        Schema::dropIfExists('documents');
        Schema::dropIfExists('issue_people');

    }
}
