<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Issue;
use Illuminate\Database\Eloquent\Factories\Factory;

class IssueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Issue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $city = $this->faker->randomElement([
            'tashkent', 'bukhara', 'nukus', 'samarkand', 'jizzakh'
        ]);

        return [
            'title'       => $this->faker->word,
            'description' => $this->faker->sentence(40),
            'state'       => $this->faker->randomElement(array_keys(Issue::getStates())),
            'location'    => $this->faker->address,
            'long'        => $this->generateLongitude($city),
            'lat'         => $this->generateLatitude($city),
            'region'      => $this->faker->country,
            'district'    => $this->faker->city,
            'category_id' => $this->faker->randomElement(Category::all('id'))
        ];
    }

    private function generateLatitude($city): string
    {
        $longs = [
            'tashkent'  => 41250000,
            'bukhara'   => 39760000,
            'nukus'     => 42460000,
            'samarkand' => 39650000,
            'jizzakh'   => 40120000,
        ];

        return $this->convert(
            $longs[$city] + $this->faker->numberBetween(0, 100000)
        );
    }

    private function generateLongitude($city): string
    {
        $lats = [
            'tashkent'  => 69200000,
            'bukhara'   => 64400000,
            'nukus'     => 59610000,
            'samarkand' => 66960000,
            'jizzakh'   => 67870000,
        ];

        return $this->convert(
            $lats[$city] + $this->faker->numberBetween(0, 100000)
        );
    }

    private function convert($number): string
    {
        return $number / 1000000;
    }
}
