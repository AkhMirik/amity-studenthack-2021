window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

export default window.axios.create({
    baseURL: '/',
    withCredentials: true
})

