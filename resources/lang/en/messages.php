<?php

return [
    'issue_created'    => 'Issue created successfully',
    'document_created' => 'Document attached to issue successfully',
];
